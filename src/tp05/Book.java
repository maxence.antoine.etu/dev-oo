package tp05;

//class
public class Book {
    private String code;
    private String title;
    private String author;
    private int publicationYear;

    //constructeur
    public Book(String code, String title, String author, int publicationYear){
        this.code = code;
        this.title = title;
        this.author = author;
        this.publicationYear = publicationYear;
    }

    //methodes
    public String toString(){
        return "[" + this.code + ": " + this.title + " " + this.author + "," + this.publicationYear + "]";
    }
    
    public String getCode(){
        return this.code;
    }

    public String getAuthor(){
        return this.author;
    }

    public String getTitle(){
        return this.title;
    }

    public int getPublicationYear(){
        return this.publicationYear;
    }

    public void setCode(String code){
        this.code = code;
    }

    public boolean borrow(int borrower){
        return ;
    }

    public boolean giveBack(){
        return true;
    }

    public boolean isAvailable(){
        return true;
    }
}

