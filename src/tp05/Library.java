package tp05;

//class
public class Library {
    private Book[] catalog;


    //methodes
    public Book getBook(String code){
        for (int i = 0; i<catalog.length; ++i){
            if(catalog[i].getCode() == code) {
                return catalog[i];
            }
        }
        return null;
    }

    public Library(Book[] catalog){
        this.catalog = catalog;
    }

    public boolean addBook(Book b){
       for (int i = 0; i<catalog.length; ++i){
            if(catalog[i].getCode() == null || catalog[i].getCode() == b.getCode()){
                return false;
            }else {
                return true;
            }
       }
       return false;
    }

    public boolean removeBook(String aCode){
        for (int i = 0; i<catalog.length; ++i){
            if (aCode == catalog[i].getCode() || aCode != null){
                return true;
            }
        }
        return false;
    }

    public boolean removeBook(Book b){
        for (int i = 0; i<catalog.length; ++i){
            if (b.getCode() == catalog[i].getCode() || b != null){
            return true;
            }
        }
        return false;
    }

    public String toString(){
        String res = "";
        for (int i = 0; i<catalog.length; ++i){
            res = catalog[i].getCode() + " " + catalog[i].getTitle() + " " + catalog[i].getAuthor() + " " + catalog[i].getPublicationYear();
        }
        return res;
    }
}
