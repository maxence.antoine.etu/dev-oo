package devOO.tp05;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Shop {
    private ArrayList<Article> catalog;

    public Shop(){
        catalog = new ArrayList<Article>();
    }

    public void addArticle(Article that){
        this.catalog.add(that);
    }

    public void addArticle(Article[] that){
        for(Article i:that){
            addArticle(i);
        }
    }
        
    public void addArticle(ArrayList<Article> that){
        this.catalog.addAll(that);
    }

    public List<PerishableArticle> getPerishables(){
        ArrayList<PerishableArticle> perishableArticles_Article = new ArrayList<PerishableArticle>();

        for (Article article : this.catalog) {
            if (article instanceof PerishableArticle) {
                perishableArticles_Article.add((PerishableArticle)article);
            }
        }
        return perishableArticles_Article;
    }


    public int getNbArticle(){
        return this.catalog.size();
    }

    public int getNbPerishableArticle(){
        return this.getPerishables().size();
    }

    public void discountPerishable(LocalDate threshold, double rate){
        for(PerishableArticle a:this.getPerishables())
            if((threshold.compareTo(a.getDeadLine())) > 1){
                a.setSalePrice(a.getSalePrice()*rate);
            }
    }

    public void discountNotPerishable(double rate){
        for (Article a: catalog) {
            if (!a.isPerishable()) {
                a.setSalePrice(a.getSalePrice()*rate);
            }
        }
    }

    public Article mostProfitable(){
        Article resu = catalog.get(0);

        for (Article a: catalog) {
            if ((a.getPurchasePrice()/a.getSalePrice()) > (resu.getPurchasePrice()/resu.getSalePrice())) {
                resu = a;
            }
        }

        return resu;
    }

    public String toString(){
        String s = "";

        for(Article i:this.catalog){
            s = s + i + '\n';
        }

        return s;
    }
}