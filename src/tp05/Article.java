package devOO.tp05;

public class Article {
    private String idRef;
    private String label;
    private final double PURCHASE_PRICE;
    private double salePrice;

    public Article(String idRef, String label, double PURCHASE_PRICE, double salePrice){
        this.idRef = idRef;
        this.label = label;
        this.PURCHASE_PRICE = PURCHASE_PRICE;
        this.salePrice = salePrice;
    }

    public Article(String idRef, String label, double PURCHASE_PRICE){
        this(idRef, label, PURCHASE_PRICE, (PURCHASE_PRICE*1.2));
    }

    public double getSalePrice(){
        return this.salePrice;
    }

    public void setSalePrice(double that){
        this.salePrice = that;
    }

    public double getPurchasePrice(){
        return this.PURCHASE_PRICE;
    }

    public double getMargin(){
        return (this.getPurchasePrice()/this.getSalePrice());
    }

    public boolean isPerishable(){
        PerishableArticle pa = new PerishableArticle(null, null, 0);
        return this.getClass().equals(pa.getClass());
    }

    public String toString(){
        return "Article " + "[" + this.idRef + ":" + this.label + "=" + this.getPurchasePrice() + "€/" + this.getSalePrice() + "€]";
    }
}
