package devOO.tp05;

import java.time.LocalDate;

public class ComicBook extends Book{
    private String illustrator;

    public ComicBook(String code, String title, String author, int publicationYear, String illustrator){
        super(code, title, author, publicationYear);
        this.illustrator = illustrator;
    }

    public int getDurationMax(){
        if((LocalDate.now().getYear() - this.getPublicationYear()) <= 2){
            return (Book.MAXDURATION-5);
        }else{
            return Book.MAXDURATION;
        }
    }
    
    public LocalDate getGiveBackDate(){
        return this.getBorrowingDate().plusDays(this.getDurationMax());
    }

    public String toString(){
        return super.toString() + " illustrator: " + this.illustrator + "]";
    }
}
