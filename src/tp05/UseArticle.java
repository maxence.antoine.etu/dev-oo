package devOO.tp05;

import java.time.LocalDate;

public class UseArticle {
    public static void main(String[] args) {
        Shop shop = new Shop();

        Article a1 = new PerishableArticle("15d458", "Pates", 4, LocalDate.of(2025, 9, 5));
        Article a2 = new Article("146sa1", "PC (RX6750, 5600, 16GO RAM DDR4 3200Mhz Vengance corssaire)", 1000, 1600);
        Article a3 = new Article("dss451", "Figurine POP Black Clover Asta Noir", 5, 20);
        Article a4 = new Article("165dsq", "GTAVI", 20, 70);
        Article a5 = new PerishableArticle("14562s", "Sauce tomate concentré", 2, LocalDate.of(2077, 5, 4));

        Article[] tab = new Article[]{a1, a2, a3};

        shop.addArticle(tab);
        shop.addArticle(a4);
        shop.addArticle(a5);

        System.out.println(shop);
    }
}
