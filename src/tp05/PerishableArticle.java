package devOO.tp05;

import java.time.LocalDate;

public class PerishableArticle extends Article{
    private LocalDate deadLine;

    public PerishableArticle(String idRef, String label, double pA, double pV, LocalDate dl){
        super(idRef, label, pA, pV);
        this.deadLine = dl;
    }

    public PerishableArticle(String idRef, String label, double pA, LocalDate dl){
        super(idRef, label, pA);
        this.deadLine = dl;
    }

    public PerishableArticle(String idRef, String label, double pA, double pV){
        super(idRef, label, pA, pV);
        this.deadLine = LocalDate.now().plusDays(10);
    }

    public PerishableArticle(String idRef, String label, double pA){
        super(idRef, label, pA);
        this.deadLine = LocalDate.now().plusDays(10);
    }
    
    public LocalDate getDeadLine(){
        return this.deadLine;
    }

    public String toString(){
        return "PerishableArticle " + super.toString().substring(8, super.toString().length()-1) + "-->" + this.deadLine + "]";
    }
}
