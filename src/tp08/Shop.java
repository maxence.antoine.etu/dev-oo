package devOO.tp08;

import java.time.LocalDate;
import java.util.ArrayList;

public class Shop {
    private ArrayList<Shelf> shelving;

    public Shop(ArrayList<Shelf> shelving){
        this.shelving = shelving;
    }

    public Shop(){
        this(new ArrayList<Shelf>());
    }

    public ArrayList<Shelf> getShelving() {
        return shelving;
    }

    public void tidy(ArrayList<IProduct> aStock){
        for(IProduct p:aStock){
            boolean place=false;
            if(p.isPerishable()){
                int cpt=0;
                while(cpt<shelving.size()&&!place){
                    if((shelving.get(cpt).isRefrigerated()) && (shelving.get(cpt).add(p))){
                        place=true;
                    }
                    cpt++;
                }
            }else{
                int cpt=0;
                while(cpt<shelving.size()&&!place){
                    if((!shelving.get(cpt).isRefrigerated()) && (shelving.get(cpt).add(p))){
                        place=true;
                    }
                    cpt++;
                }
            }
        }
    }

    public ArrayList<IProduct> closeBestBeforeDate(int nbDays){
        ArrayList<IProduct> close = new ArrayList<IProduct>();

        for (Shelf shelf : this.shelving) {
            for (IProduct product : shelf.getShelves()) {
                if (product.isPerishable()) {
                    Food current = (Food)product;
                    if (current.isBestBefore(LocalDate.now().plusDays(nbDays))) {
                        close.add(current);
                    }
                }
            }
        }

        return close;
    }

    @Override
    public String toString() {
        return "Shop [shelving=" + shelving + "]";
    }
}
