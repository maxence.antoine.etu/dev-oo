package devOO.tp08;

/**
 * Participant
 */
public class Participant {
    private String name;
    private Participant partner;

    /**
     * @param name
     */
    public Participant(String name) {
        this.name = name;
    }
    
    /**
     * @return
     */
    public String getName(){
        return this.name;
    }

    /**
     * @return
     */
    public String getPartnerName(){
        return this.partner.getName();
    }

    /**
     * @return
     */
    public boolean isMatched(){
        return this.partner != null;
    }

    /**
     * @param p
     * @return
     */
    public boolean isMatchedWith(Participant p){
        if (isMatched()) {
            return this.partner.equals(p);
        }

        return false;
    }

    /**
     * @param p
     * @return
     */
    public boolean matchWith(Participant p){
        if (isMatched()) {
            return false;
        }else{
            partner = p;
            p.setPartner(this);
            return true;
        }
    }

    /**
     * 
     */
    public void breakOff(){
        this.partner = null;
    }


    public String toString(){
        return "[" + this.name + " -> " + this.partner + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((name == null) ? 0 : name.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Participant other = (Participant) obj;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }

    /**
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param partner
     */
    public void setPartner(Participant partner) {
        this.partner = partner;
    }
    
}