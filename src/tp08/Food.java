package tp08;

import java.time.LocalDate;

public class Food implements IProduct, Comparable<Food>{
    
    private String label;
    private double price;
    private LocalDate bestBeforeDate;
    private static int cmp;

    public Food(String label, double price, LocalDate bestBeforeDate){
        if (label == null){
            this.label = "refUnknown" + cmp;
            ++cmp;
        }else{
            this.label = label;
        }
        this.price = price;
        this.bestBeforeDate = bestBeforeDate; 
        
    }

    public Food(String label, double price){
        if (label == null){
            this.label = "refUnknownXXX";
        }else{
            this.label = label;
        }
        this.price = price;
        this.bestBeforeDate = LocalDate.now().plusDays(10);
        
    }

    public String getLabel(){
        return this.label;
    }

    public LocalDate getBestBeforDate(){
        return this.bestBeforeDate;
    }

    public double getPrice() {
        return this.price;
    }

    public void setLabel(String label){
        this.label = label;
    }

    public void setPrice(double price){
        this.price = price;
    }

    public void setBestBeforeDate(LocalDate bestBeforeDate){
        this.bestBeforeDate = bestBeforeDate;
    }


    public boolean isPerishable(){
        if(this.bestBeforeDate == null){
            return false;
        }
        return true;
    }

    public String toString(){
        return "[" + this.label + "=" + this.price + " -> before " + this.bestBeforeDate + "]";
    }

    public boolean isBestBefore(LocalDate aDate){
        if(this.bestBeforeDate.isAfter(aDate)){
            return false;
        }
        return true;
    }

    public int compareTo(Food other){
       return this.bestBeforeDate.compareTo(other.bestBeforeDate);
    }

    

}
