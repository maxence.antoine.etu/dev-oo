package devOO.tp08;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

public class BreedingGround {
    private HashSet<Participant> applicants = new HashSet<Participant>();
    private final Random RAND = new Random();

    /**
     * @return
     */
    public HashSet<Participant> getApplicants() {
        return applicants;
    }

    /**
     * @param p
     * @return
     */
    public boolean registration(Participant p){
        if (this.applicants.contains(p)) {
            return false;
        }else{
            this.applicants.add(p);
            return true;
        }
    }

    /**
     * @return
     */
    public List<Participant> loners(){
        ArrayList<Participant> res = new ArrayList<Participant>();

        for (Participant p : this.applicants) {
            if (!p.isMatched()) {
                res.add(p);
            }
        }

        return res;
    }

    /**
     * @return
     */
    public List<Participant> lonersCleansing(){
        ArrayList<Participant> res = new ArrayList<Participant>();

        for (Participant p : this.applicants) {
            if (!p.isMatched()) {
                res.add(p);
            }
        }

        this.applicants.removeAll(res);
        return res;
    }

    /**
     * 
     */
    public void forcedMatching(){
        ArrayList<Participant> lonner = (ArrayList<Participant>) this.loners();

        while(lonner.size() > 1){
            int nb1 = RAND.nextInt(loners().size());
            int nb2;
            do{
                nb2 = RAND.nextInt(loners().size());
            }while (nb1 == nb2);

            Participant p1 = lonner.get(nb1);
            Participant p2 = lonner.get(nb2);

            p1.matchWith(p2);
            lonner.remove(p1);
            lonner.remove(p2);
        }
    }

    /**
     * @param anotherBreedingGround
     * @return
     */
    public List<Participant> cheaters(BreedingGround anotherBreedingGround){
        ArrayList<Participant> cheaters = new ArrayList<Participant>();

        for (Participant participant : this.applicants) {
            if (anotherBreedingGround.applicants.contains(participant)) {
                cheaters.add(participant);
            }
        }

        return cheaters;
    }

    /**
     * @param cheater
     */
    public void isolateCheater(Participant cheater){
        cheater.breakOff();
    }

    /**
     * @param anotherBreedingGround
     */
    public void cheatersCleansing(BreedingGround anotherBreedingGround){
        for (Participant participant : this.cheaters(anotherBreedingGround)) {
            isolateCheater(participant);
        }
    }

    /**
     * @param anotherBreedingGround
     * @return
     */
    public boolean possibleMerging(BreedingGround anotherBreedingGround){
        List<Participant> cheaters = this.cheaters(anotherBreedingGround);

        return this.applicants.contains(cheaters);
    }

    /**
     * @param anotherBreedingGround
     */
    public void merging(BreedingGround anotherBreedingGround){
        this.applicants.addAll(anotherBreedingGround.applicants);
    }

    /**
     * @param anotherBreedingGround
     */
    public void securedMerging(BreedingGround anotherBreedingGround){
        if (this.possibleMerging(anotherBreedingGround)) {
            merging(anotherBreedingGround);
        }
    }

    public void clear() {
        this.applicants.clear();
    } 
}
