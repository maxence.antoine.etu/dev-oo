package tp08;

import java.util.*;

public class Shelf {
    
    private boolean refrigerated;
    private int capacityMax;
    private ArrayList<IProduct> shelves = new ArrayList<>();

    public Shelf(boolean refrigerated, int capacityMax){
        this.refrigerated = refrigerated;
        this.capacityMax = capacityMax;
    }

    public ArrayList<IProduct> getShelves(){
        return this.shelves;
    }

    public boolean isFull(){
        if(this.capacityMax == 0){
            return false;
        }
        return true;
    }

    public boolean isEmpty(){
        if(this.capacityMax == 0){
            return true;
        }
        return false;
    }

    public boolean isRefrigerated(){
        if(this.refrigerated == false){
            return false;
        }
        return true;
    }

    public String toString(){
        return "[" + this.refrigerated + " : " + this.capacityMax + " -> " + getShelves() + "]";
    }
}
