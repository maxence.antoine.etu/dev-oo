package tp07;

public class PhoneNumber {
    private int countryCode;
    private int areaCode;
    private int sectorCode;
    private int one;
    private int two;
    private int three;

    public PhoneNumber(int countryCode, int areaCode, int sectorCode, int one, int two, int three){
        this.countryCode = countryCode;
        this.areaCode = areaCode;
        this.sectorCode = sectorCode;
        this.one = one;
        this.two = two;
        this.three = three;
    }

    public String standardFormat(){
        return "Le Numéro est : " + this.areaCode + this.sectorCode + this.one + this.two + this.three;
    }

    public String internationalFormat(){
        return "Le Numéro est : +" + this.countryCode;
    }

    public boolean equals(PhoneNumber other){
        return this == other;
    }

    public String toString(){
        return internationalFormat() + standardFormat();
    }
}
