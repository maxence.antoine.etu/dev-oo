package tp07;

public class ProPhoneNumber extends PhoneNumber {
    private String numbers;
    private UniversityDepartment dep;


    public ProPhoneNumber(String numbers, UniversityDepartment dep){
        this.numbers = numbers;
        this.dep = dep;
    }

    public UniversityDepartment getDepartment(){
        return this.dep;
    }

    public String internToString(){
        return this.numbers + "(IUT)";
    }

    public String externToString(){
        return this.internationalFormat() + " (Institut Universitaire de Technologies)"
    }

    public boolean equals(Object obj){
        
    }
}
