package devOO.tp10;

public class PrioritySchedulingQueue<T extends IPriority> extends SchedulingQueue<T>{

    public void addElement(T elem) {
        if (theQueue.isEmpty()) {
            theQueue.add(elem);
        }else{
            int i=0;
            while (i<theQueue.size() && theQueue.get(i).getPriority() < elem.getPriority()) {
                ++i;
            }
            theQueue.add(i, elem);
        }
    }

    public T highestPriority() {
        if (!this.theQueue.isEmpty()) {
            T res = this.theQueue.getFirst();
            for (T t : this.theQueue) {
                if (res.getPriority() > t.getPriority()){
                    res = t;
                }
            }
            this.theQueue.remove(res);
            return res;
        }

        return null;
    }
}
