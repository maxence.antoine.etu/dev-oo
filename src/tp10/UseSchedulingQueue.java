package devOO.tp10;

import java.util.LinkedList;

import devOO.tp05.Book;

public class UseSchedulingQueue {

    public static void premierScenario(){
        Book b1 = new Book("a", "Ce chier dessus pour les nuls","Bardella", 0);
        Book b2 = new Book("d", "e","f", 1);
        Book b3 = new Book("g", "h","i", 2);

        SchedulingQueue<Book> sq = new SchedulingQueue<>();

        sq.addElement(b3);
        System.out.println(sq);
        sq.addElement(b2);
        System.out.println(sq);
        sq.addElement(b1);
        System.out.println(sq);
        
        sq.highestPriority();
        System.out.println(sq);
        sq.highestPriority();
        System.out.println(sq);
        sq.highestPriority();
        System.out.println(sq);
    }

    public static void deuxiemeScenario(){
        Task t1 = new Task("a", 1);
        Task t2 = new Task("b", 2);
        Task t3 = new Task("c", 3);

        SchedulingQueue<Task> sq = new SchedulingQueue<>();

        sq.addElement(t3);
        System.out.println(sq);
        sq.addElement(t2);
        System.out.println(sq);
        sq.addElement(t1);
        System.out.println(sq);
        
        sq.highestPriority();
        System.out.println(sq);
        sq.highestPriority();
        System.out.println(sq);
        sq.highestPriority();
        System.out.println(sq);


    }

    public static void main(String[] args) {
        premierScenario();
        deuxiemeScenario();
    }
}
