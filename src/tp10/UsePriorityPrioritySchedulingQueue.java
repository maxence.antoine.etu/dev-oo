package devOO.tp10;

public class UsePriorityPrioritySchedulingQueue {

    public static void premierScenario(){
        Task t1 = new Task( "t1", 6);
		Task t2 = new Task( "t2", 4);
		Task t3 = new Task( "t3", 2);

        PrioritySchedulingQueue<Task> sq=new PrioritySchedulingQueue<>();

        sq.addElement(t3);
        System.out.println(sq);
        sq.addElement(t2);
        System.out.println(sq);
        sq.addElement(t1);
        System.out.println(sq);
        
        sq.highestPriority();
        System.out.println(sq);
        sq.highestPriority();
        System.out.println(sq);
        sq.highestPriority();
        System.out.println(sq);


    }

    public static void main(String[] args) {
        premierScenario();
    }
}
