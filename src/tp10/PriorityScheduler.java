package devOO.tp10;

import java.util.ArrayList;

public class PriorityScheduler implements IScheduler<IPriority>{
    protected ArrayList<SchedulingQueue<IPriority>> queue;

    public PriorityScheduler(int nb){
        this.queue = new ArrayList<>(nb);
    }

    public int size(int priority){
        if (!this.queue.get(priority).isEmpty()) {
            return this.queue.get(priority).size();
        }
        return 0;
    }

    @Override
    public int size() {
        int res = 0;
        if (!this.isEmpty()) {
            for (SchedulingQueue<IPriority> s : this.queue) {
                if (!s.isEmpty()) {
                    res += s.size();
                }
            }
        }
        return res;
    }

    public String toString(){
        String res = "PriorityQueue:" + '\n';
        for (int i = 0; i < 5; ++i) {
            res += this.queue.get(i).toString() + '\n';
        }
        return res;
    }

    @Override
    public boolean isEmpty() {
        boolean res = true;

        for (SchedulingQueue<IPriority> schedulingQueue : this.queue) {
            if (!schedulingQueue.isEmpty()) {
                res = false;
            }
        }
        
        return res;
    }


    @Override
    public void addElement(IPriority el) {
        this.queue.get(el.getPriority()).addElement(el);
    }

    @Override
    public IPriority highestPriority() {
        if (!this.isEmpty()) {
            for (SchedulingQueue<IPriority> schedulingQueue : this.queue) {
                if (!schedulingQueue.isEmpty()) {
                    return schedulingQueue.highestPriority();
                }
            }
        }
        return null;
    }
}
