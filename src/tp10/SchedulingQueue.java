package tp10;

import java.util.LinkedList;

public class SchedulingQueue<T> implements IScheduler<T> {
    
    private int id;
    protected LinkedList<T> theQueue;
    private String Qlabel;

    public SchedulingQueue(int id, LinkedList<T> theQueue, String Qlabel){
        this.id = id;
        this.theQueue = theQueue;
        this.Qlabel = Qlabel;
    }

    public void addElement(T elt){
        theQueue.add(elt);
    }

    public T highestPriority(){
        return theQueue.getFirst();

    }

    public boolean isEmpty(){
        return theQueue.isEmpty();
    }

    public int size(){
        return theQueue.size();
    }

    public int getID(){
        return this.id;
    }

    public String toString(){
        if (this.theQueue.isEmpty()){
            return this.Qlabel + " -> []";
        }
        return this.Qlabel + " -> [" + 
    }

}
