package tp10;

public interface IScheduler<T> {
    
    public void addElement(T elt);

    public T highestPriority();

    public boolean isEmpty();

    public int size();
}
