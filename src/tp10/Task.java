package tp10;

public class Task implements IPriority {
    private int priority;
    private String label;

    public Task(String label, int priority){
        this.label = label;
        this.priority = priority;
    }

    public int getPriority(){
        return this.priority;
    }

    public String getLabel(){
        return this.label;
    }

    public void setLabel(String label){
        this.label = label;
    }

    public void setPriority(int priority){
        this.priority = priority;
    }

    public String toString(){
        return "Task:" + this.label + "(" + this.priority + ")";
    }



}
