import controlepkg1.A;
import controlepkg2.B;
import controlepkg2.sspkg.C;
import controlpkg1.D;

public class Main {
   
    public static void main(String[] args) {
        A a = new A("Jobert");
        B b = new B();
        C c = new C("Huslain", 2);
        D d = new D();
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
        System.out.println(d.getSomeText());
    }
    
}
