public class A{

    static final String LABEL = "42";
    String label;

    // Constructeur
    public A() {
        this.label = LABEL;
    }

    public A(String label) {
        this.label = label;
    }

    public String toString() {
        return this.label ;
    }

    public static void main(String[] args) {
        A a1 = new A();
        A a2 = new A("à choisir");
        System.out.println(a1);
        System.out.println(a2);
    }

}