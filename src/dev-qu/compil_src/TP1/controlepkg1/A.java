package compil_src.controlepkg1;

public class A {
    private String aLabel;

    public A(String aLabel) {
        this.aLabel = aLabel;
    }
    @Override
    public String toString() {
        return "A [aLabel=" + aLabel + "]";
    }
}
