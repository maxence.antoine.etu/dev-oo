package fourth;

public class A{

    static final String LABEL = "42";
    String label;

    // Constructeur
    public A() {
        this.label = LABEL;
    }

    public A(String label) {
        this.label = label;
    }

    public String toString() {
        return this.label ;
    }

}