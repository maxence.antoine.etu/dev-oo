import TP3.Person;

public class Student1 {
    public static final int DEFAULT_DURATION = 20;
    private int duration;

    //constructeur
    public Student1(String name, String forename){
        this.name = name;
        this.forename = forename;
        this.duration = DEFAULT_DURATION;
    }

    public Student1(String name, String forename, int duration){
        this.name = name;
        this.forename = forename;
        this.duration = duration;
    }

}
