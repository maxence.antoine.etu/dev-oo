public class Person {

    private String name;
    private String forename;

    //constructeur
    public Person(String name, String forename){
        this.name = name;
        this.forename = forename;
    }

    //methodes
    public String getName(){
        return this.name;
    }

    public String getForename(){
        return this.forename;
    }

    public String toString(){
        return "<" + this.forename + "> " + "<" + this.name + ">" ;
    }
}

