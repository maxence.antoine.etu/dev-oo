package tpQU.tp05;

public class UseStock {
    public static void main(String[] args) {
        Machine m = new Machine("Poste de Alice");
        m.addElem(new Piece("écran"));
        m.addElem(new Piece("clavier"));
        m.addElem(new Piece("souris"));
        
        Element E1 = new Element("unité centrale");
        E1.addElem(new Piece("carte mère"));
        E1.addElem(new Piece("extension mémoire"));
        E1.addElem(new Piece("disque scsi"));
        
        Element E2 = new Element("carte graphique");
        E2.addElem(new Piece("extension mémoire"));
        E2.addElem(new Piece("processeur graphique"));
        
        E1.addElem(E2);
        
        m.addElem(E1);
        
        m.addElem(new Piece("webcam"));

        System.out.println(m.toString());
    }
}