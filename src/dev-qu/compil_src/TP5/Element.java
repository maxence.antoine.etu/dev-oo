package tpQU.tp05;

import java.util.ArrayList;

public class Element {
    private String name;
    protected ArrayList<Element> components;

    public Element(String name){
        this.name = name;
        components = new ArrayList<>();
    }

    

    public void addElem(Element e2) {
        this.components.add(e2);
    }



    public String getName() {
        return name;
    }



    public ArrayList<Element> getComponents() {
        return components;
    }
}
