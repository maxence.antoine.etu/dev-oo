package tpQU.tp05;

public class Piece extends Element{
    private int nbInv;
    private static int nbauto;

    public Piece(String name){
        super(name);
        this.nbInv = ++nbauto;
    }

    public int getNbInv(){
        return this.nbInv;
    }

    public String getName() {
        return this.getName();
    }

    public String toString(){
        String res = "";
        for (Element e : this.components) {
            res += '\t' + e.toString() + '\n';
        }
        return res;
    }    
}
