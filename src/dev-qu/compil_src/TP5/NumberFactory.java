package tpQU.tp05;

public class NumberFactory {

    public static Number number(int num){
        return (Number)num;
    }

    public static Number number(long num){
        return (Number)num;
    }

    public static Number number(float num){
        return (Number)num;
    }

    public static Number number(double num){
        return (Number)num;
    }

    public static Number number(String num){
        try{
            return (Number) Double.parseDouble(num);
        }catch(NumberFormatException e){
            e.getStackTrace();
        }finally{
            return (Number) 0;
        }
    }

    public static Number number(int num, int num2){
        return (Number) new Fraction(num, num2).doubleValue();
    }

}
