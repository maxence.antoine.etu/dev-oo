package tpQU.tp05;

public class Fraction extends Number{
    private int num;
    private int denom;

    public Fraction(int num, int denom){
        this.num = num;
        this.denom = denom;
    }

    public double doubleValue(){
        return (double)this.num/(double)this.denom;
    }

    public float floatValue(){
        return (float)this.num/(float)this.denom;
    }

    public int intValue(){
        return this.num/this.denom;
    }

    public long longValue(){
        return (long)this.num/(long)this.denom;
    }

    @Override
    public String toString() {
        return "Fraction [num=" + num + ", denom=" + denom + "]";
    }

}
