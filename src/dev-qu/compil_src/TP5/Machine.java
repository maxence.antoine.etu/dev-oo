package tpQU.tp05;

public class Machine extends Element{
    private String name;

    public Machine(String name){
        super(name);
    }

    public String getName() {
        return this.name;
    }

    public void addElem(Element o){
        this.components.add(o);
    }

    public String toString(){
        String res = super.getName() + ":" + '\n';
        for (Element e : this.components) {
            res += e.toString() + '\n';
        }
        return res;
    }    
}
