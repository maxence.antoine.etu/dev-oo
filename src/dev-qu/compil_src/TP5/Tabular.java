package tpQU.tp05;

public class Tabular {
    private Number[] tab;

    public Tabular(int max){
        this.tab = new Number[max];
    }

    public void set(int r, Number n){
        this.tab[r] = n;
    }

    public Number max(){
        Number max = 0;

        for(int i = 0; i < this.tab.length; ++i){
            if (this.tab[i].doubleValue() > max.doubleValue()) {
                max = this.tab[i];
            }
        }

        return max;
    }

    public String toString(){
        String res = "Tab ->";

        for(int i = 0; i < this.tab.length; ++i){
            res += " " + this.tab[i].toString();
        }

        return res;
    }

    public static void main(String[] args) {
        Tabular tab = new Tabular(10);
        tab.set(0,NumberFactory.number(0));
        tab.set(1,NumberFactory.number((long) 1));
        tab.set(2,NumberFactory.number((double) 2));
        tab.set(3,NumberFactory.number((float) 3));
        tab.set(4,NumberFactory.number(40,1));
        tab.set(5,NumberFactory.number((long) 5));
        tab.set(6,NumberFactory.number((double) 6));
        tab.set(7,NumberFactory.number((float) 7));
        tab.set(8,NumberFactory.number("2fbn"));
        tab.set(9,NumberFactory.number((long) 9));
        System.out.println("Max : " + tab.max());
        System.out.println(tab.toString());
    }

}
