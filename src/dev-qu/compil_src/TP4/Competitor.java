package tpQU.tp04;

public class Competitor {
    private final String NAME;
    private Country nationality;
    private Team team;
    private static int nbAuto = 0;
    private int id;

    public Competitor(String name, Country country, Team team){
        this.NAME = name;
        this.nationality = country;
        this.team = team;
        this.id = ++nbAuto;
    }

    @Override
    public String toString() {
        return this.id + "-" + this.NAMEname + "(" + this.nationality + ") -> " + this.team;
    }
}
