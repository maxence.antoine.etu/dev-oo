package tpQU.tp04;

public enum Container {
    BLISTER(1), BOX(10), CRATE(50);

    private int capacity;

    private Container(int cap){
        this.capacity = cap;
    }

    public int getCapacity(){
        return this.capacity;
    }
}
