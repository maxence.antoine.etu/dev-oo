package tpQU.tp04;

public enum Country {
    FRANCE("FR"), GERMANY("GE"), RUSSIA("RS"), SWEDEN("SE"), AUSTRIA("AT"), ITALY("IT");

    private final String VC;

    private Country(String vc){
        this.VC = vc;
    }

    public String getVC(){
        return this.VC;
    }

    public String getName(){
        return this.toString();
    }
}
