package compil_src.TP4;

import tpQU.tp04.*;

public class Ex2 {
    public static int[] tab = {17, 12, 15, 38, 29, 157, 89, -22, 0, 5};
    public static int division(int index, int divisor) {
        return tab[index]/divisor;
    }
    public static void statement() {
        int x, y;
        x = LocalKeyboard.readInt("Write the index of the integer to divide: ");
        y = LocalKeyboard.readInt("Write the divisor: ");

        try {
            System.out.println("The result is: " + division(x,y));
        } catch (ArithmeticException ae) {
            System.out.println("Vous ne pouvez pas diviser par 0");
        } catch (NullPointerException npe) {
            System.out.println("L'objet que vous essayez d'obtnir n'est pas initialisé");
        } catch (ArrayIndexOutOfBoundsException aiobe) {
            System.out.println("La case du tableau que vous essayez ");
        } catch (KeyboardException ke) {
            System.out.println("Autre chose qu'un objet entier est lu");
        }

    }
}
