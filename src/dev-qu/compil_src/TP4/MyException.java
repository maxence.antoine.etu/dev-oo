package compil_src.TP4;

public class MyException extends Exception {
    public MyException() {}
    public MyException(String msg) {super(msg);}

    public void example2() throws MyException {
        throw new MyException();
    }

    public void doItAgain() throws MyException {
        throw new MyException();
    }

    public void example3() throws MyException {
        try{
            doItAgain();
        }catch (Exception e) {
            processing();
        }
    }
    public void processing() throws MyException {
        throw new MyException();
    }

    public int example4() {
        int data = 0;
        try {
            data = getData();
        } catch (NullPointerException e) {e.printStackTrace();}
        return data;
    }


    public int getData() throws NullPointerException {
        throw new NullPointerException();
    }

    public void example5() {
        try {
            doItFinally();
        } catch (RuntimeException e) {e.printStackTrace();}
    }

    public int doItFinally() {
        throw new RuntimeException();
    }

    public static void main(String[] args) {
        int k;
        try {
            k = 1/Integer.parseInt(args[0]);
        }
        catch(ArithmeticException e) {System.err.println("Arithmetic " + "");}
        catch(ArrayIndexOutOfBoundsException e) {System.err.println("Index " + e);}
        catch(RuntimeException e) {System.err.println("Runtime " + e);
        }
    }
}
