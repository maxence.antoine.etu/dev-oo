package tpQU.tp04;

public class Thing {
    private String name;
    private int quantity;
    private Container container;

    public Thing(String name, int quantity, Container container){
        this.name = name;
        if (container.getCapacity() >= quantity) {
            this.quantity = quantity;
        }else{
            this.quantity = 0;
        }
        this.container = container;
    }

    private void setQuatity(int that){
        if (that > this.container.getCapacity()) {
            throw new ExtendedException();
        }
        this.quantity = that;
    }

    public boolean transferToLargerContainer(){
        if (this.container.equals(Container.BLISTER)) {
            this.container = Container.BOX;
            return true;
        }else if(this.container.equals(Container.BOX)){
            this.container = Container.CRATE;
            return true;
        }
        return false;
    }

    public boolean add(int quantity){
        if ((quantity + this.quantity) > this.container.getCapacity()) {
            if (this.transferToLargerContainer()) {
                if ((this.quantity + quantity) < this.container.getCapacity()) {
                    this.quantity += quantity;
                    return true;
                }else{
                    return false;
                }
            }else{
                throw new ExtendedException();
            }
        }else{
            this.quantity += quantity;
            return true;
        }
    }

    @Override
    public String toString() {
        return "Thing [name=" + name + ", quantity=" + quantity + ", container=" + container + "]";
    }

    public static void main(String[] args) {
        Thing ch1 = new Thing("uneThing", 3, Container.BOX);
        System.out.println(ch1);
        ch1.add(20);
        System.out.println(ch1 + "\n");
        Thing ch2 = new Thing("maThing", 12, Container.BOX);
        System.out.println(ch2);
        ch2.add(60);
        System.out.println(ch2 + "\n");
    }

}