/**
 * @author Maxence Antoine
 * 
 * v1
 * 
 * 13/03/24
 * 
 * Copyright
 */
package TP2.compil_src;

//class
public class WarriorCard {
    //init
    private String name;
    private int strength;
    private int agility;

    //constructeur
    public WarriorCard(String name, int s, int ag){
        this.name = name;
        this.strength = s;
        this.agility = ag;
    }

    //methodes
    public boolean equals(Object obj){
        return this.name.equals(obj);
    }

    public int compareStrength(WarriorCard other){
        if(this.strength > other.strength){
            return Integer.parseInt(">") + 0;
        }else if (this.strength < other.strength) {
            return Integer.parseInt("<") + 0;
        }
        else{
            return 0;
        }
    }

    public int compareAgility(WarriorCard other){
        if(this.agility > other.agility){
            return Integer.parseInt(">") + 0;
        }else if (this.agility < other.agility) {
            return Integer.parseInt("<") + 0;
        }
        else{
            return 0;
        }
    }

    public String toString(){
        return this.name + "[S=" + this.strength + ",A=" + this.agility + "]";
    }

    
}