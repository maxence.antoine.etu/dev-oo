package tp04;

public class Student {
    private Person pers;
    private double[] grades;
    
    //constructeur
    private Student(Person pers, double[] grades){
        this.pers = pers;
        this.grades = grades;
    }

    public Student(String forename, String name, double[] grades){
        this(new Person(forename, name), grades);
    }

    public Student(String forename, String name){
        this(forename, name, new double[0] );
    }

    //methodes
    public String getForename(){
        return this.pers.getForename();
    }

    public String getName(){
        return this.pers.getName();
    }

    public int getId(){
        return this.pers.getId();
    }

    public void setForename(String forename){
        this.pers.setForename(forename);
    }

    public void setName(String name){
        this.pers.setName(name);
    }
    private String doubleArraytoString(double[] tab){
        String rep = "[ ";
        for (int i = 0 ; i<tab.length; ++i){
            rep = rep + tab[i] + " ";
        }
        return rep + "]";
    }


    public String toString(){
        return "Student [" + this.getId() + ": " + this.getForename() + " " + this.getName() + " = " + doubleArraytoString(grades) + "]";
    }

    

    public boolean equals(Object other){
        if(this == other) return true;
        if(other == null) return false;
        if(getClass() != other.getClass()) return false;
        Student other1 = (Student) other;
        if(this.getForename() == null){
            if(other1.getForename() != null) return false;
        } else if (!this.getForename().equals(other1.getForename())) return false;
        if(this.getName() == null){
            if(other1.getName() != null) return false;
        } else if (!this.getName().equals(other1.getName())) return false;
        if(this.getId() != other1.getId()) return false;
        return true;
    }

    public double getAverage(double[] grades){
        double som = 0;
        for(int i = 0; i<grades.length; ++i){
            som = som+grades[i];
        }
        return som/grades.length;
    }

    private void fillList(double[] listCopy, double[] listPaste){
        if(listCopy.length <= listPaste.length){
            for(int i = 0; i<listCopy.length; ++i){
                listPaste[i] = listCopy[i];
            }
        }
    }

    public void addGrade (double aGrade){
        double[] newlist = new double[this.grades.length+1];
        fillList(this.grades, newlist);
        this.grades = newlist;
        this.grades = aGrade;
    }
}
