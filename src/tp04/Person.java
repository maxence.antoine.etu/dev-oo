package tp04;

public class Person {
    private int id;
    private String forename;
    private String name;
    public static int cpt = 0;

    //constructeur
    Person(String forename, String name){
        this.forename = forename;
        this.name = name;
        this.id = ++cpt;
    }

    public void setForename(String forename){
        this.forename = forename;
    }

    public void setName(String name){
        this.name = name;
    }

    //methodes

    public String getForename(){
        return this.forename;
    }

    public String getName(){
        return this.name;
    }

    public int getId(){
        return this.id;
    }

    public String toString(){
        return this.id + ": " + this.forename + " " + this.name; 
    }

    public boolean equals(Object obj){
        if(this == obj){
            return true;
        } 
        if(obj == null){
            return false;
        }
        return false; 
    }
}
