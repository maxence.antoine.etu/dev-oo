public class DicePlayer {

    private String name;
    private int totalValue;
    private int nbDiceRolls;

    // Constructeur
    DicePlayer(String name){
        this.name = name;
    }

    // Méthodes
    public void play(Dice dice6){
        dice6.roll();
        this.totalValue = this.totalValue + dice6.getValue();
        ++this.nbDiceRolls;
    }

    public String toString() {
        return this.name + ": " + this.totalValue + " points en " + this.nbDiceRolls + " coups."; 
    }
}

