import java.util.Random;

public class Dice {
    
    private int numberSides;
    private static final Random RAND = new Random();
    private int value;

    // Constructeur
    public Dice(int numberSides){
        if (numberSides > 0){
            this.numberSides = numberSides;
        }else {
            this.numberSides = 1;
        }
        
    }

    // Méthodes
    public void roll(){
        this.value = this.RAND.nextInt(numberSides)+1;
    }

    public String toString(int value) {
        return "Le dé a fait " + value;
    }

    public int getValue() {
        return this.value;
    }

}
