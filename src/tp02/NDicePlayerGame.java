public class NDicePlayerGame {
    private DicePlayer[] players;

    public NDicePlayerGame(int numPlayers) {
        this.players = new DicePlayer[numPlayers];
        for (int i = 0; i < numPlayers; i++) {
            players[i] = new DicePlayer(String.valueOf(i));
        }
    }

    public DicePlayer[] winner() {
        int minRolls = Integer.MAX_VALUE;
        int countWinners = 0;
        for (DicePlayer player : players) {
            if (player.nbDiceRolls < minRolls) {
                minRolls = player.nbDiceRolls;
                countWinners = 1;
            } else if (player.nbDiceRolls == minRolls) {
                countWinners++;
            }
        }
        DicePlayer[] winners = new DicePlayer[countWinners];
        int index = 0;
        for (DicePlayer player : players) {
            if (player.nbDiceRolls == minRolls) {
                winners[index++] = player;
            }
        }

        return winners;
    }

    public static void main(String[] args) {
        int numPlayers = Integer.parseInt(args[0]);
        NDicePlayerGame game = new NDicePlayerGame(numPlayers);
        Dice dice = new Dice(6);
        for (DicePlayer player : game.players) {
            while (player.totalValue < 20) {
                player.play(dice);
            }
        }
        System.out.println("Les vainqueurs:");
        for (DicePlayer winner : game.winner()) {
            System.out.println(winner.name + ": " + winner.totalValue + " points en " + winner.nbDiceRolls + " coups.");
        }
    }
}
