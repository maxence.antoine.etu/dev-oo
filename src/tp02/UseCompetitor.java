public class UseCompetitor {
	public static void main(String[] args) {
		Competitor alice = new Competitor(1, 45, 15, 20);
		Competitor bruno = new Competitor(1, 45, 15, 20);
		Competitor clement = new Competitor(2, 32, 12, 45);
		Competitor dora = new Competitor(2, 34, 12, 45);
		System.out.println("Alice:" + alice.display() + " Bruno:" + bruno.display() + "->" + alice.isFaster(bruno));
		System.out.println("Alice:" + alice.display() + " null:" + null + "->" + alice.isFaster(null));
		System.out.println("Alice:" + alice.display() + " Alice:" + alice.display() + "->" + alice.isFaster(alice));
		System.out.println("Alice:" + alice.display() + " Clement:" + clement.display() + "->" + alice.isFaster(clement));
		System.out.println("Clement:" + clement.display() + " Dora:" + dora.display() + "->" + clement.isFaster(dora));
	}
}
