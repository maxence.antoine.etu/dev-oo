public class TwoDicePlayerGame {
    private DicePlayer player1;
    private DicePlayer player2;

    public TwoDicePlayerGame(DicePlayer player1, DicePlayer player2) {
        this.player1 = player1;
        this.player2 = player2;
    }

    public DicePlayer winner() {
        if (player1.isWinning(player2)) {
            return player1;
        } else {
            return player2;
        }
    }

    public static void main(String[] args) {
        DicePlayer alice = new DicePlayer("Alice");
        DicePlayer bob = new DicePlayer("Bob");
        TwoDicePlayerGame game = new TwoDicePlayerGame(alice, bob);

        Dice dice = new Dice(6);

        while (alice.totalValue < 20 && bob.totalValue < 20) {
            alice.play(dice);
            bob.play(dice);
        }

        System.out.println("Le gagnant est " + game.winner());
    }
}
