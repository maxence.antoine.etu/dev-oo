// Auteur : Maxence Antoine

public class Competitor {
    private String numberSign;
    private int time;
    private int score;


    // Constructeur
    Competitor(int numberSign, int score, int min, int sec){
        if(numberSign >= 1 && numberSign <= 100 && min >= 0 && min <= 60 && sec >= 0 && sec <= 60 && score >= 0 && score <= 50){
            this.numberSign = "No" + numberSign;
            this.time = min*60 + sec;
            this.score = score;
        }else {
            this.numberSign = null;
        }
    }

    // Méthodes
    String display(){
        if(this.numberSign == null) {
            return "[<invalide>, " + this.score + " points, " + this.time + " s]";
        }else {
            return "[" + this.numberSign + "," + this.score + " points, " + this.time + " s]";
        }
    }

    boolean equals(Competitor other){
        if (other != null) {
            if(this.numberSign.equals(other.numberSign) && this.score == other.score) {
                return true;
            }    
        }
        return false;
    }

    boolean isFaster(Competitor other) {
        if(other == null) {
            return true;
        }
        if(this.time > other.time){
            return true;
        }
        return false;
    }


    public static void main(String[] args) {
        Competitor[] tab = new Competitor[100];
        tab[0] = new Competitor(1, 45, 15, 20);
        tab[1] = new Competitor(1, 45, 12, 45);
        tab[2] = new Competitor(5, 12, 13, 59);
        tab[3] = new Competitor(12, 12, 15, 70);
        tab[4] = new Competitor(32, 75, 15, 20);
        for(int i = 0; i< tab.length; ++i) {
            if(tab[i] != null) {
                System.out.println(tab[i].display());
                System.out.println(tab[0].isFaster(tab[i]));
            }
            
        }
    }
}
