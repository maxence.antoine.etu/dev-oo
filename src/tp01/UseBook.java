//Auteur : Maxence Antoine

class UseBook {
    public static void main(String[] args){
        Book livre1 = new Book("Stephen King", "It", 1986);
        Book livre2 = new Book("George Orwell", "1984", 1949);
        Book livre3 = new Book("Jules Verne", "Voyage au centre de la Terre", 1864);
        Book biblio[] = new Book[] {livre1, livre2, livre3};
        for(int i = 0; i < biblio.length ; ++i ) {
           System.out.println(biblio[i].print()); 
        }
        
    }
}