//Auteur: Maxence Antoine

class Book {
    // class attributes
    String author;
    String title;
    int year;
    // constructor
    Book(String author, String title, int year) {
        this.author = author;
        this.title = title;
        this.year = year;
    }
    // methods
    String getAuthor() {
        return this.author;
    }
    String getTitle() {
        return this.title;
    }
    String print() {
        return author + " a écrit " + title + " en " + year;
    }

    public static void main(String[] args){
        Book livre = new Book("Edwin A. Abbott", "Flatland", 1884);
        System.out.println(livre.print());
        
    }
    

}

