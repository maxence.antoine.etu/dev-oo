//Auteur: Maxence Antoine

public class RandomSequence {
    public static void main(String[] args) {
        if (args.length <2 || args.length > 3 || args[0].contains(".")) {
            System.err.println("Correct usage : <nbElt> <maxVal> [INTEGER|REAL]");
        }else {
            int nbElt;
            int maxVal;
            double maxValD;
            if (args.length == 3 && args[2].equals("REAL")){
                nbElt = Integer.parseInt(args[0]);
                maxValD = Double.parseDouble(args[1]);
                for (int i = 0; i<nbElt; ++i) {
                    System.out.println((Math.random()*maxValD)+1);
                } 
            }
            else {
                nbElt = Integer.parseInt(args[0]);
                maxVal = Integer.parseInt(args[1]);
                for (int i = 0; i<nbElt; ++i) {
                    System.out.println((int)(Math.random()*maxVal)+1);
                }
            }
        }
        
    }
}
