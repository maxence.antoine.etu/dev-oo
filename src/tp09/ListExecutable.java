package devOO.tp09;

import java.io.File;
import java.io.IOException;

import util.HierarchyCreation;

public class ListExecutable {
    private final static String FS = System.getProperty("file.separator");
    
    public static void main(String[] args) {
		String path = System.getProperty("user.home") + FS + "hierarchy";
		try {
			if(!(new File(path)).exists()){
				HierarchyCreation.hierarchyCreation(path);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		printExecutable(path);
	}

	public static void printExecutable(String path) {
		File file = new File(path);
		for (File f : file.listFiles()) {
			if (f.canExecute() && !f.isHidden() && !f.isDirectory()) {
				System.out.println(f.getAbsolutePath());
			}else if(!f.isHidden() && f.isDirectory()){
				printExecutable(f.getAbsolutePath());
			}
		}
	}
}
