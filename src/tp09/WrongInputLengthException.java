package devOO.tp09;

public class WrongInputLengthException extends Exception{
    public WrongInputLengthException(){}
    public WrongInputLengthException(String msg) {super(msg);}
}
