package devOO.tp09;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.InputMismatchException;

public class Validation {
    private final static String FS = System.getProperty("file.separator");

    public static boolean checkFile(String filename) throws IOException{
        boolean isValid = true;

        try(BufferedReader br = new BufferedReader(new FileReader("res"+FS+"tp09"+FS+"testScan.csv"))){
            String c = "";
    		String[] arraySplit = new String[3];
    		while((c = br.readLine())!=null){
    			System.out.println(c);
    			arraySplit = c.split(";");
    			switch(Integer.parseInt(arraySplit[0])){
                    case 1:
                        for(int i = 2; i<2+Integer.parseInt(arraySplit[1]);++i){
                            try{
                                Integer.parseInt(arraySplit[i]);
                            }
                            catch(InputMismatchException e){
                                throw new InputMismatchException();
                            }
                        }

                }
                    
    		}
        }
        catch(FileNotFoundException fnfe) {
			fnfe.printStackTrace();
			System.exit(0);
		}

        return isValid;
    }
}
