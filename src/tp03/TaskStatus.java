public enum TaskStatus {
    TODO, ONGOING, DELAYED, FINISHED;
}
