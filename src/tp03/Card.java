/** 
 * @author Maxence Antoine
*/
public class Card {
    private Color color;
    private Rank rank;

    //constructeur
    public void Card(Color color, Rank rank) {
        this.color = color;
        this.rank = rank;
    }

    public void Card(String color, String rank) {
        this.color = Color.valueOf(color);
        this.rank = Rank.valueOf(rank);
    }

    //méthodes
    public Color getColor(){
        return this.color;
    }

    public Rank getRank(){
        return this.rank;
    }

    public int compareRank(Card carte){
        return Integer.parseInt(this.rank.name()) - Integer.parseInt(carte.rank.name());
    }

    public int compareColor(Card carte){
        if (carte.color.name().equals(this.color.name()) == true) {
            return 1;
        }else{
            return -1;
        }
    }

    //public boolean isBefore(Card carte)

    
    public boolean equals(Card carte){
        return (carte.compareColor(carte) == carte.compareRank(carte));
    }


    public String toString(){
        return this.rank.name() + this.color.name();
    }
}