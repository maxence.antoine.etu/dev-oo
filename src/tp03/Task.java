import java.time.LocalDate;

public class Task {
    private String idTask;
    private LocalDate creationDate;
    private LocalDate deadline;
    private TaskStatus state;
    private String description;
    private int duration;


    //getter
    public String idTask(){
        return this.idTask;
    }

    public LocalDate creationDate(){
        return this.creationDate;
    }

    public LocalDate deadline(){
        return this.deadline;
    }

    public TaskStatus state(){
        return this.state;
    }

    public String description(){
        return this.description;
    }

    public int duration(){
        return this.duration;
    }


    //constructeur
    Task(String description, int duration){
        this.description = description;
        this.duration = duration;
    }

    Task(String description, LocalDate creationDate, LocalDate deadline, int duration)

}
