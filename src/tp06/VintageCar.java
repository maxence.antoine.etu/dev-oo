package devOO.tp06;

import java.time.LocalDate;

public class VintageCar extends Car{
    public static final double temporalDropRate = 0.0;
    public static final double mileageDropRate = 0.0;

    public VintageCar(String brand, LocalDate onRoad, double pPrice, LocalDate onSale, double salePrice, int km) {
        super(brand, onRoad, pPrice, onSale, salePrice, km);
    }
    
    public VintageCar(String brand, LocalDate onRoad, double pPrice, int km) {
        super(brand, onRoad, pPrice, km);
    }

    public double getTemporaldroprate(){
        return VintageCar.temporalDropRate;

    }
}
