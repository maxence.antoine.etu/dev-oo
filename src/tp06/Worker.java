package tp06;

import java.time.LocalDate;

public class Worker extends Employee{
    private final static double BY_UNIT = 5.0;
    private int units;


    public Worker(String name, LocalDate hiringDate, int units){
        super(name, hiringDate);
        this.units = units;
    }

    public String getTitle(){
        return super.getTitle();
    }

    public double getWages(){
        return super.getWages()+this.units*BY_UNIT;
    }

    public String toString(){
        return super.toString();
    }
}
