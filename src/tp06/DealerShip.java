package devOO.tp06;

import java.time.LocalDate;
import java.util.ArrayList;

public class DealerShip {
    ArrayList<Car> autos;

    public DealerShip(){
        autos = new ArrayList<Car>();
    }

    public void addCar(Car v){
        this.autos.add(v);
    }

    public Car createCar(String brand, LocalDate onRoad, double pPrice, LocalDate onSale, double salePrice, int km){
        if ((LocalDate.now().getYear() - onRoad.getYear()) > 40) {
            return new VintageCar(brand, onRoad, pPrice, onSale, salePrice, km);
        }else{
            return new Car(brand, onRoad, pPrice, onSale, salePrice, km);
        }
    }

    public Car createCar(String brand, LocalDate onRoad, double pPrice, int km){
        return createCar(brand, onRoad, pPrice, LocalDate.now(), pPrice, km);
    }
}