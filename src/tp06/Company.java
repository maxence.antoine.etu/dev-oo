package devOO.tp06;

import java.time.LocalDate;
import java.util.ArrayList;

public class Company {
    ArrayList<Employee> staff;

    public Company(){
        staff = new ArrayList<Employee>();
    }

    public void addEmployee(Employee that){
        this.staff.add(that);
    }

    public void supprEmployee(int i){
        this.staff.remove(i);
    }

    public void supprEmployee(Employee e){
        this.staff.remove(e);
    }

    public String toString(){
        return this.staff.toString();
    }

    public int getNbEmployee(){
        return this.staff.size();
    }

    public int getNbSalesPerson(){
        int res = 0;

        for(int idx = 0; idx < this.staff.size(); ++idx){
            if (this.staff.get(idx) instanceof Salesperson) {
                res++;
            }
        }

        return res;
    }

    public int getNbWorker(){
        int res = 0;

        for(int idx = 0; idx < this.staff.size(); ++idx){
            if (this.staff.get(idx) instanceof Worker) {
                res++;
            }
        }

        return res;
    }

    public void firing(LocalDate fatefulDate){
        ArrayList<Employee> temp = new ArrayList<Employee>();

        for (int idx = 0; idx < this.staff.size(); ++idx) {
            if ((this.staff.get(idx).getHirinDate().isAfter(fatefulDate))){
                temp.add(this.staff.get(idx));
            }
        }

        this.staff.removeAll(temp);
    }

    public void firing(){
        ArrayList<Employee> temp = new ArrayList<Employee>();

        for (int idx = 0; idx < this.staff.size(); ++idx) {
            if (!this.staff.get(idx).objectiveFulfilled()) {
                temp.add(this.staff.get(idx));
            }
        }
        
        this.staff.removeAll(temp);
    }
}