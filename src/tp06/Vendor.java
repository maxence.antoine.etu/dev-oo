package tp06;

import java.time.LocalDate;

public class Vendor extends Salesperson{
    private final static double POURCENTAGE = 0.20;
    private final static int BONUS = 400;

    public Vendor(String name, LocalDate hiringDate, double turnover){
        super(name, hiringDate, turnover);
    }

    public String getTitle(){
        return super.getTitle();
    }

    public double getWages(){
        return super.getWages();
    }
}
