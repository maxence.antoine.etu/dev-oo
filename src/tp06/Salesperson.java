package tp06;

import java.time.LocalDate;

public abstract class Salesperson extends Employee {
    private double turnover;

    public Salesperson(String name, LocalDate hiringDate, double turnover){
        super(name,hiringDate);
        this.turnover = turnover;
    }

    public double getTurnover(){
        return this.turnover;
    }

    public String toString(){
        return getName() + " " + getTitle() + " " + getTurnover();
    }
}
