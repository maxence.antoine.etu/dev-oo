package tp06;

import java.time.LocalDate;

public class TravellingSalesperson extends Salesperson{
    private final static double POURCENTAGE = 0.20;
    private final static int BONUS = 800;

    public TravellingSalesperson(String name, LocalDate hiringDate, double turnover){
        super(name, hiringDate, turnover);
    }

    public String getTitle(){
        return super.getTitle();
    }

    public double getWages(){
        return super.getWages();
    }
}