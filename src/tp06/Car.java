package tp06;

import java.time.LocalDate;

public class Car{
    private final static double TEMPORALDROPRATE = 0.001;
    private final static double MILEAGEDROPRATE = 0.002;
    private final String BRAND;
    private final LocalDate ONROAD;
    private final double PURCHASEPRICE;
    private LocalDate onSale;
    private double salePrice;
    private int mileage;

    public Car(String brand, LocalDate onRoad, double pPrice, LocalDate onSale, double sPrice, int km){
        this.BRAND = brand;
        this.ONROAD = onRoad;
        this.PURCHASEPRICE = pPrice;
        this.onSale = onSale;
        this.salePrice = sPrice;
        this.mileage = km;
    }

    public Car(String brand, LocalDate onRoad, double pPrice, int km){
        this.BRAND = brand;
        this.ONROAD = onRoad;
        this.PURCHASEPRICE = pPrice;
        this.mileage = km;
    }
}